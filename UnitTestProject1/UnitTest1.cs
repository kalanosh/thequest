﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TheQuest;
namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            Direction TestDirection = TheQuest.Direction.Up;
            Console.Write(TestDirection+"\nClockwise:"+ TestDirection.CounterClockwise());
            Assert.AreEqual(TestDirection.CounterClockwise(), Direction.Left);
            Console.WriteLine("\n"+TestDirection + "\nClockwise:" + TestDirection.CounterClockwise().CounterClockwise());
            Assert.AreEqual(TestDirection.CounterClockwise().CounterClockwise(), Direction.Down);
            Console.WriteLine("\nClockwise:" + TestDirection.CounterClockwise().CounterClockwise().CounterClockwise());
            

        }
    }
}
