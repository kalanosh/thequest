﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace TheQuest
{
    class BluePotion : Weapon, IPotion
    {
        public BluePotion(Game game, Point Location)
            : base(game, Location)
        {
            this.Used = false;
        }

        public bool Used { get; private set; }

        public override string Name
        {
            get { return "Blue Potion"; }
        }

        public override void Attack(Direction direction, Random random)
        {
            game.IncreasePlayerHealth(5, random);
            this.Used = true;
        }
    }
}
