﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace TheQuest
{
    class Sword: Weapon
    {
        public Sword(Game game, Point location)
            :base(game, location)
        {}

        public override string Name { get { return "Sword";}}

        
        public override void Attack(Direction direction, Random random)
        {
            if (DamageEnemy(direction, 10, 3, random))
                return;
            else if (DamageEnemy(direction.Clockwise(), 10, 3, random))
                return;
            else
                DamageEnemy(direction.CounterClockwise(), 10, 3, random);
        }

    }
}
