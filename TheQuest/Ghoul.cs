﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;


namespace TheQuest
{
    class Ghoul:Enemy
    {
        public Ghoul(Game game, Point Location)
            :base(game, Location, 10)
        {

        }

        public override void Move(Random random)
        {
            if (Hitpoints > 0)
            {
                if(random.Next(3)>0)
                {
                    this.Move(FindPlayerDirection(game.PlayerLocation), game.Boundaries);
                }

            }
        }
    }
}

