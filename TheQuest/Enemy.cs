﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace TheQuest
{
    abstract class Enemy:Mover
    {
        private const int NearPlayerDistance = 25;
        public int Hitpoints { get; private set; }

        /// <summary>
        /// Property that returns true if Hitpoints equal to or less than Zero
        /// </summary>
        public bool Dead
        {
            get
            {
                if (Hitpoints <= 0) return true;
                else return false;
            }
        }

        /// <summary>
        /// Creates enemy using location, game object and hitpoints
        /// </summary>
        /// <param name="game"></param>
        /// <param name="Location"></param>
        /// <param name="Hitpoints"></param>
        public Enemy(Game game, Point Location, int Hitpoints)
            :base(game, Location)
        {
            // TODO: Complete member initialization
            this.Hitpoints = Hitpoints;
        }

        /// <summary>
        /// Class to be implemented by child
        /// </summary>
        /// <param name="random"></param>
        public abstract void Move(Random random);

        /// <summary>
        /// Enenmy class calulating hit and subtracting from Hitpoints
        /// </summary>
        /// <param name="maxDamage"></param>
        /// <param name="random"></param>
        public void Hit(int maxDamage, Random random)
        {
            Hitpoints -= random.Next(1, maxDamage);
        }

        /// <summary>
        /// Returns if object if near the player
        /// </summary>
        /// <returns></returns>
        protected bool NearPlayer()
        {
            return (Nearby(game.PlayerLocation, NearPlayerDistance));
        }

        /// <summary>
        /// Finds the player cardial direction and set directionToMove to direction player is in.
        /// </summary>
        /// <param name="playerLocation"></param>
        /// <returns></returns>
        protected Direction FindPlayerDirection(Point playerLocation)
        {
            Direction directionToMove;
            if (playerLocation.X > location.X + 10)
                directionToMove = Direction.Right;
            else if (playerLocation.X < location.X - 10)
                directionToMove = Direction.Left;
            else if (playerLocation.Y < Location.Y - 10)
                directionToMove = Direction.Up;
            else
                directionToMove = Direction.Down;
            return directionToMove;
        }

    }
}
