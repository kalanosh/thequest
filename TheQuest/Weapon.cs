﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace TheQuest
{
    abstract class Weapon: Mover
    {
        public bool PickedUp { get; private set; }

        public Weapon(Game game, Point location)
            :base(game, location)
        {
            PickedUp = false;
        }

        public void PickUpWeapon()
        { 
            PickedUp = true;
        }

        public abstract string Name { get; }

        public abstract void Attack(Direction direction, Random random);

        public bool Nearby(Point locationA, Point locationB, int distance)
        {
            if (Math.Abs(locationA.X - locationB.X) < distance && (Math.Abs(locationA.Y - locationB.Y) < distance))
                return true;
            else
                return false;
        }

        public Point Move(Direction direction, Point target, Rectangle boundaries)
        {
            this.location = target;
            return base.Move(direction, boundaries);
        }

        protected bool DamageEnemy(Direction direction, int radius, int damage, Random random)
        {
            Point target = game.PlayerLocation;
            for (int distance = 0; distance < radius / 2; distance++)
            {
                foreach (Enemy enemy in game.Enemies)
                {
                    if (Nearby(enemy.Location, target, distance))
                    {
                        enemy.Hit(damage, random);
                        return true;
                    }
                }
                target = Move(direction, target, game.Boundaries);
            }
            return false;

        }


    }
}
