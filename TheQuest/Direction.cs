﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace TheQuest
{
   
    internal enum Direction
    {
        Up,
        Right,
        Down,
        Left,
    }

    internal static  class DirectionExtenstion
    {
        internal static Direction Clockwise(this Direction direction)
        {
            if ((int)direction == 3)
                return Direction.Up;
            else
                return (Direction)((int)direction + 1);
        }

        internal static Direction CounterClockwise(this Direction direction)
        {
            if ((int)direction == 0)
                return Direction.Left;
            else
                return (Direction)((int)direction - 1);
        }
    }
}
