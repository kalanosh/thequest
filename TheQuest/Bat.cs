﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace TheQuest
{
    class Bat: Enemy
    {
        public Bat(Game game, Point Location)
            :base(game, Location, 6)
        {

        }

        public override void Move(Random random)
        {
            if (this.Hitpoints > 0)
            {
                if (random.Next(10) > 4)
                    this.Move(FindPlayerDirection(game.PlayerLocation), game.Boundaries);
                else
                    this.Move((Direction)random.Next(4), game.Boundaries);

            }
            else
            {
 
            }
        }


    }
}
