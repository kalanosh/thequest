﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;                    

namespace TheQuest
{
    abstract class Mover
    {
        private const int MoveInterval = 10;
        protected Point location;
        public Point Location { get {return location; } }
        protected Game game;

        /// <summary>
        /// Sets-up mover base class, only required Game class that controlling and a Point for location for mover to start
        /// </summary>
        /// <param name="game">Primary Game class object instance </param>
        /// <param name="Location">Location to place object during intitalization</param>
        public Mover(Game game, Point Location)
        {
            this.game = game;
            this.location = Location;
        }

        /// <summary>
        /// Check distanct to a location (Point)
        /// </summary>
        /// <param name="locationToCheck">Point object to check against</param>
        /// <param name="distance">distance to within check for</param>
        /// <returns>Returns true is the two locations are within the given distance</returns>
        public bool Nearby(Point locationToCheck, int distance)
        {
            if(Math.Abs(location.X - locationToCheck.X) < distance && (Math.Abs(location.Y - locationToCheck.Y) < distance))
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// Attempts to see if move is valid and return new move value based on direction
        /// </summary>
        /// <param name="direction">Direction enum indicating desire direction</param>
        /// <param name="boundaries">Rectangle object that defines game boundaries </param>
        /// <returns>Returns valid move within game boundaries</returns>
        public Point Move(Direction direction, Rectangle boundaries)
        {
            Point newLocation = location;
            switch (direction)
            {
                case Direction.Up:
                    if(newLocation.Y - MoveInterval >= boundaries.Top)
                        newLocation.Y -= MoveInterval;
                    break;
                case Direction.Down:
                    if(newLocation.Y + MoveInterval <= boundaries.Bottom)
                        newLocation.Y += MoveInterval;
                    break;
                case Direction.Left:
                    if(newLocation.X - MoveInterval >= boundaries.Left)
                        newLocation.X -= MoveInterval;
                    break;
                case Direction.Right:
                    if(newLocation.X + MoveInterval >= boundaries.Right)
                        newLocation.X += MoveInterval;
                    break;
                default: break;
            }

            return newLocation;
        }
    }
}
