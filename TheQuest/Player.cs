﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace TheQuest
{
    class Player: Mover
    {
        /// <summary>
        /// Player's currently equipped weapon(Weapon)
        /// </summary>
        private Weapon equippedWeapon;

        /// <summary>
        /// Player class Hitpoints int
        /// </summary>
        public int HitPoints { get; private set; }

        /// <summary>
        /// Container List of object Weapon in invetory
        /// </summary>
        private List<Weapon> inventory = new List<Weapon>();

        /// <summary>
        /// Iterates over List inventory of Weapon class and returns each Weapon.Name(String) in a generic container IEnumerable
        /// </summary>
        public IEnumerable<string> Weapons
        {
            get
            {
                var names = new List<string>();
                foreach (Weapon weapon in inventory)
                    names.Add(weapon.Name);
                return names;
            }
        }

        /// <summary>
        /// Player inherits and constructs with game object and a location to start
        /// </summary>
        /// <param name="game">primary game object</param>
        /// <param name="location">Point location to start</param>
        public Player(Game game, Point location )
            :base(game, location)
        {
            HitPoints = 10;
        }
        public void Move(Direction direction)
        {
            base.location = Move(direction, game.Boundaries);
            if (!game.WeaponInRoom.PickedUp)
            {

                
            }
        }

        /// <summary>
        /// player object check list of weapons for item and 'equips' it
        /// </summary>
        /// <param name="weaponName">Weapon object to equipd</param>
        public void Equip(string weaponName)
        {
            foreach (Weapon weapon in inventory)
                if (weapon.Name == weaponName)
                    equippedWeapon = weapon;

        }

        /// <summary>
        /// When a enenmy class hits a player it calls hit, takes max damage and randomly assign it against HitPoint (int)
        /// </summary>
        /// <param name="maxDamage">enemy class provides int max damage that can occur</param>
        /// <param name="random">Random class that being used by game object</param>
        public void Hit(int maxDamage, Random random)
        {
            HitPoints -= random.Next(1, maxDamage);

        }

        /// <summary>
        /// When a player is affected by effect to increase health
        /// </summary>
        /// <param name="health">amount of health the item heals for</param>
        /// <param name="random">Random object used by primary Game object</param>
        public void IncreaseHealth(int health, Random random)
        {
            HitPoints += random.Next(1, health);
            CheckInventory();
        }

        /// <summary>
        /// When the player class attack an enemy class
        /// </summary>
        /// <param name="direction">Direction to prerform attack</param>
        /// <param name="random">Random object used by primary class</param>
        public void Attack(Direction direction, Random random)
        {

        }

        public void CheckInventory()
        {
            var ToRemove = new List<Weapon>();
            foreach (Weapon weapon in inventory)
            {
                var IsPotion = weapon as IPotion;
                if (IsPotion != null && IsPotion.Used)
                    ToRemove.Add(weapon);            
            }
            
            foreach (Weapon weapon in ToRemove)
            {
                inventory.Remove(weapon);
            }
        }

    }
}
