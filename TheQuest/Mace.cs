﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
namespace TheQuest
{
    class Mace: Weapon
    {
        public Mace(Game game, Point Location)
            :base(game, Location)
        {

        }

        public override string Name
        {
            get { return "Mace"; }
        }

        public override void Attack(Direction direction, Random random)
        {
             if (DamageEnemy(direction, 10, 3, random))
                return;
            else if (DamageEnemy(direction.Clockwise(), 10, 3, random))
                return;
             else if (DamageEnemy(direction.Clockwise().Clockwise(), 10, 3, random))
                 return;
            else
                 DamageEnemy(direction.CounterClockwise(), 10, 3, random);

        }
    }
}
