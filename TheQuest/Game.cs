﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace TheQuest
{
    class Game
    {
        public IEnumerable<Enemy> Enemies { get; private set; }
        public Weapon WeaponInRoom { get; private set; }

        private Player player;
        public Point PlayerLocation { get { return player.Location; } }
        public int PlayerHitPoints { get { return player.HitPoints; } }
        public IEnumerable<string> PlayerWeapons { get { return player.Weapons; } }
        private int level = 0;
        public int Level { get { return level; } }

        private Rectangle boundaries;
        public Rectangle Boundaries { get { return boundaries; } }

        /// <summary>
        /// Creates the game object and sets up its game paly boundaries and creates the Player Object
        /// </summary>
        /// <param name="boundaries">Size of game area</param>
        public Game(Rectangle boundaries)
        {
            this.boundaries = boundaries;
            player = new Player(this, new Point(boundaries.Left + 10, boundaries.Top + 70));

        }

        /// <summary>
        /// Moves player in indicated direction and then moves enemies.
        /// </summary>
        /// <param name="direction"> Direction indicated by the player, should be passed from Form</param>
        /// <param name="random"> Random object currently being used.</param>
        public void Move(Direction direction, Random random)
        {
            player.Move(direction);
            foreach (Enemy enemy in Enemies)
                enemy.Move(random);
        }

        /// <summary>
        /// Tell player to equip weapon or item.
        /// </summary>
        /// <param name="weaponName">Name of item to equip</param>
        /// <remarks>Ensure item exist in player innventory before doing an Equip as it only calls Player.Equip</remarks>
        public void Equip(string weaponName)
        {
            player.Equip(weaponName);
        }

        /// <summary>
        /// Passes and item and checks the players inventory and returns if item is in inventory.
        /// </summary>
        /// <param name="weaponName"> Item name to check for.</param>
        /// <returns>Return true is item exist in player inventory.</returns>
        public bool CheckPlayerInventory(string weaponName)
        {
            return player.Weapons.Contains(weaponName);
        }

        /// <summary>
        /// Passes damage information from enemy succesful hit to player.Hit
        /// </summary>
        /// <param name="maxDamage"> Max Damage to Player in Int</param>
        /// <param name="random">Pass random object being used currently</param>
        /// <remarks> Do not create a new Random, or the number could become similar everytime.</remarks>
        public void HitPlayer(int maxDamage, Random random)
        {
            player.Hit(maxDamage, random);
        }

        /// <summary>
        /// Increase the Player health by specfified amount
        /// </summary>
        /// <param name="health">Amount of health to increase by</param>
        /// <param name="random">Pass in the currently use Random object</param>
        /// <seealso cref="Random"/>
        public void IncreasePlayerHealth(int health, Random random)
        {
            player.IncreaseHealth(health, random);
        }

        /// <summary>
        /// Player attacks an enemy in the given direction and the enemies get a turn to move
        /// </summary>
        /// <param name="direction">Direction specified to attack, given from Form</param>
        /// <param name="random">Pass in currently use Random object </param>
        public void Attack(Direction direction, Random random)
        {
            player.Attack(direction, random);
            foreach (Enemy enemy in Enemies)
                enemy.Move(random);
        }

        /// <summary>
        /// Gets and returns a random location within the game's boundaries
        /// </summary>
        /// <param name="random">Pass in currently use Random object</param>
        /// <returns>returns a <see cref="System.Drawing.Point"/> object within game boundaries defined by <see cref="System.Drawing.Rectangle"/></returns>
        private Point GetRandomLocation(Random random)
        {
            return new Point(boundaries.Left + random.Next(boundaries.Right / 10 - boundaries.Left / 10) * 10, boundaries.Top + random.Next(boundaries.Bottom / 10 - boundaries.Top / 10) * 10);
        }

        public void NewLevel(Random random)
        {
            level++;
            switch (level)
            {
                case 1:
                    {
                        Enemies = new List<Enemy>() { new Bat(this, GetRandomLocation(random)),};
                        WeaponInRoom = new Sword(this, GetRandomLocation(random));
                        break;
                    }
                case 2:
                    {
                        Enemies = new List<Enemy>() { new Ghost(this, GetRandomLocation(random)), };
                        WeaponInRoom = new BluePotion(this, GetRandomLocation(random));
                        break;
                    }
                case 3:
                    {
                        Enemies = new List<Enemy>() { new Ghoul(this, GetRandomLocation(random)), };
                        WeaponInRoom = new Bow(this, GetRandomLocation(random));
                        break;
                    }
                case 4:
                    {
                        Enemies = new List<Enemy>() { new Bat(this, GetRandomLocation(random)), new Ghost(this, GetRandomLocation(random)), };
                        bool PickedUpBow = false;
                        bool PickedUpPotion = false;
                        //Check if player already has a bow, if so set flag and don't spawn bow on level
                        foreach (string weapon in player.Weapons)
                        {
                            if (weapon == "Bow")
                                PickedUpBow = true;
                            if (weapon == "Blue Potion")
                                PickedUpPotion = true;

                        }

                        if (!PickedUpBow)
                            WeaponInRoom = new Bow(this, GetRandomLocation(random));
                        else if(!PickedUpPotion)
                            WeaponInRoom = new BluePotion(this, GetRandomLocation(random))
                        break;
                    }
                case 5:
                    {
                        Enemies = new List<Enemy>() { new Bat(this, GetRandomLocation(random)), new Ghoul(this, GetRandomLocation(random)), };
                        WeaponInRoom = new RedPotion(this, GetRandomLocation(random));
                        break;
                    }
                case 6:
                    {
                        Enemies = new List<Enemy>() { new Ghost(this, GetRandomLocation(random)), new Ghoul(this, GetRandomLocation(random)), };
                        WeaponInRoom = new Mace(this, GetRandomLocation(random));
                        break;
                    }
                case 7:
                    {
                        Enemies = new List<Enemy>() { new Bat(this, GetRandomLocation(random)), new Ghost(this, GetRandomLocation(random)), 
                            new Ghoul(this, GetRandomLocation(random)), };
                        bool PickedUpMace = false;
                        bool PickedUpPotion = false;
                        foreach (string weapon in player.Weapons)
                        {
                            if(weapon == "Mace")
                                PickedUpMace = true;

                            if (weapon == "Red Potion")
                                PickedUpPotion = true;
                        }

                        if (!PickedUpMace)
                            WeaponInRoom = new Mace(this, GetRandomLocation(random));
                        else if(!PickedUpPotion)
                             WeaponInRoom = new RedPotion(this, GetRandomLocation(random));

                        break;

                    }
                case 8:
                    {
                        Ap
                    }
             }


         }
     }
    
}
