﻿namespace TheQuest
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox_sword = new System.Windows.Forms.PictureBox();
            this.pictureBox_mace = new System.Windows.Forms.PictureBox();
            this.pictureBox_redpotion = new System.Windows.Forms.PictureBox();
            this.pictureBox_bluepotion = new System.Windows.Forms.PictureBox();
            this.pictureBox_bow = new System.Windows.Forms.PictureBox();
            this.pictureBox_ghost = new System.Windows.Forms.PictureBox();
            this.pictureBox_player = new System.Windows.Forms.PictureBox();
            this.pictureBox_ghoul = new System.Windows.Forms.PictureBox();
            this.pictureBox_bat = new System.Windows.Forms.PictureBox();
            this.pictureBox_InvSword = new System.Windows.Forms.PictureBox();
            this.pictureBox_InvRedPotion = new System.Windows.Forms.PictureBox();
            this.pictureBox_InvBow = new System.Windows.Forms.PictureBox();
            this.pictureBox_InvBluePotion = new System.Windows.Forms.PictureBox();
            this.pictureBox_InvMace = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.labelPlayer = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.labelPlayerHP = new System.Windows.Forms.Label();
            this.labelBatHP = new System.Windows.Forms.Label();
            this.labelGhostHP = new System.Windows.Forms.Label();
            this.labelGhoulHP = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_sword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_mace)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_redpotion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_bluepotion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_bow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_ghost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_player)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_ghoul)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_bat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_InvSword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_InvRedPotion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_InvBow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_InvBluePotion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_InvMace)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox_sword
            // 
            this.pictureBox_sword.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox_sword.Image = global::TheQuest.Properties.Resources.sword;
            this.pictureBox_sword.Location = new System.Drawing.Point(194, 95);
            this.pictureBox_sword.Name = "pictureBox_sword";
            this.pictureBox_sword.Size = new System.Drawing.Size(50, 50);
            this.pictureBox_sword.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_sword.TabIndex = 0;
            this.pictureBox_sword.TabStop = false;
            this.pictureBox_sword.Visible = false;
            // 
            // pictureBox_mace
            // 
            this.pictureBox_mace.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox_mace.Image = global::TheQuest.Properties.Resources.mace;
            this.pictureBox_mace.Location = new System.Drawing.Point(586, 95);
            this.pictureBox_mace.Name = "pictureBox_mace";
            this.pictureBox_mace.Size = new System.Drawing.Size(50, 50);
            this.pictureBox_mace.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_mace.TabIndex = 1;
            this.pictureBox_mace.TabStop = false;
            this.pictureBox_mace.Visible = false;
            // 
            // pictureBox_redpotion
            // 
            this.pictureBox_redpotion.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox_redpotion.Image = global::TheQuest.Properties.Resources.potion_red;
            this.pictureBox_redpotion.Location = new System.Drawing.Point(530, 95);
            this.pictureBox_redpotion.Name = "pictureBox_redpotion";
            this.pictureBox_redpotion.Size = new System.Drawing.Size(50, 50);
            this.pictureBox_redpotion.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_redpotion.TabIndex = 2;
            this.pictureBox_redpotion.TabStop = false;
            this.pictureBox_redpotion.Visible = false;
            // 
            // pictureBox_bluepotion
            // 
            this.pictureBox_bluepotion.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox_bluepotion.Image = global::TheQuest.Properties.Resources.potion_blue;
            this.pictureBox_bluepotion.Location = new System.Drawing.Point(474, 95);
            this.pictureBox_bluepotion.Name = "pictureBox_bluepotion";
            this.pictureBox_bluepotion.Size = new System.Drawing.Size(50, 50);
            this.pictureBox_bluepotion.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_bluepotion.TabIndex = 3;
            this.pictureBox_bluepotion.TabStop = false;
            this.pictureBox_bluepotion.Visible = false;
            // 
            // pictureBox_bow
            // 
            this.pictureBox_bow.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox_bow.Image = global::TheQuest.Properties.Resources.bow;
            this.pictureBox_bow.Location = new System.Drawing.Point(418, 95);
            this.pictureBox_bow.Name = "pictureBox_bow";
            this.pictureBox_bow.Size = new System.Drawing.Size(50, 50);
            this.pictureBox_bow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_bow.TabIndex = 4;
            this.pictureBox_bow.TabStop = false;
            this.pictureBox_bow.Visible = false;
            // 
            // pictureBox_ghost
            // 
            this.pictureBox_ghost.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox_ghost.Image = global::TheQuest.Properties.Resources.ghost;
            this.pictureBox_ghost.Location = new System.Drawing.Point(362, 95);
            this.pictureBox_ghost.Name = "pictureBox_ghost";
            this.pictureBox_ghost.Size = new System.Drawing.Size(50, 50);
            this.pictureBox_ghost.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_ghost.TabIndex = 5;
            this.pictureBox_ghost.TabStop = false;
            this.pictureBox_ghost.Visible = false;
            // 
            // pictureBox_player
            // 
            this.pictureBox_player.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox_player.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox_player.Image = global::TheQuest.Properties.Resources.player;
            this.pictureBox_player.Location = new System.Drawing.Point(138, 95);
            this.pictureBox_player.Name = "pictureBox_player";
            this.pictureBox_player.Size = new System.Drawing.Size(50, 50);
            this.pictureBox_player.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_player.TabIndex = 6;
            this.pictureBox_player.TabStop = false;
            this.pictureBox_player.Visible = false;
            // 
            // pictureBox_ghoul
            // 
            this.pictureBox_ghoul.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox_ghoul.Image = global::TheQuest.Properties.Resources.ghoul;
            this.pictureBox_ghoul.Location = new System.Drawing.Point(306, 95);
            this.pictureBox_ghoul.Name = "pictureBox_ghoul";
            this.pictureBox_ghoul.Size = new System.Drawing.Size(50, 50);
            this.pictureBox_ghoul.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_ghoul.TabIndex = 7;
            this.pictureBox_ghoul.TabStop = false;
            this.pictureBox_ghoul.Visible = false;
            // 
            // pictureBox_bat
            // 
            this.pictureBox_bat.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox_bat.Image = global::TheQuest.Properties.Resources.bat;
            this.pictureBox_bat.Location = new System.Drawing.Point(250, 95);
            this.pictureBox_bat.Name = "pictureBox_bat";
            this.pictureBox_bat.Size = new System.Drawing.Size(50, 50);
            this.pictureBox_bat.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_bat.TabIndex = 8;
            this.pictureBox_bat.TabStop = false;
            this.pictureBox_bat.Visible = false;
            // 
            // pictureBox_InvSword
            // 
            this.pictureBox_InvSword.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox_InvSword.Image = global::TheQuest.Properties.Resources.sword1;
            this.pictureBox_InvSword.Location = new System.Drawing.Point(138, 617);
            this.pictureBox_InvSword.Name = "pictureBox_InvSword";
            this.pictureBox_InvSword.Size = new System.Drawing.Size(80, 80);
            this.pictureBox_InvSword.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_InvSword.TabIndex = 9;
            this.pictureBox_InvSword.TabStop = false;
            this.pictureBox_InvSword.Visible = false;
            // 
            // pictureBox_InvRedPotion
            // 
            this.pictureBox_InvRedPotion.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox_InvRedPotion.Image = global::TheQuest.Properties.Resources.potion_red;
            this.pictureBox_InvRedPotion.Location = new System.Drawing.Point(224, 617);
            this.pictureBox_InvRedPotion.Name = "pictureBox_InvRedPotion";
            this.pictureBox_InvRedPotion.Size = new System.Drawing.Size(80, 80);
            this.pictureBox_InvRedPotion.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_InvRedPotion.TabIndex = 10;
            this.pictureBox_InvRedPotion.TabStop = false;
            this.pictureBox_InvRedPotion.Visible = false;
            // 
            // pictureBox_InvBow
            // 
            this.pictureBox_InvBow.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox_InvBow.Image = global::TheQuest.Properties.Resources.bow;
            this.pictureBox_InvBow.Location = new System.Drawing.Point(310, 617);
            this.pictureBox_InvBow.Name = "pictureBox_InvBow";
            this.pictureBox_InvBow.Size = new System.Drawing.Size(80, 80);
            this.pictureBox_InvBow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_InvBow.TabIndex = 11;
            this.pictureBox_InvBow.TabStop = false;
            this.pictureBox_InvBow.Visible = false;
            // 
            // pictureBox_InvBluePotion
            // 
            this.pictureBox_InvBluePotion.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox_InvBluePotion.Image = global::TheQuest.Properties.Resources.potion_blue;
            this.pictureBox_InvBluePotion.Location = new System.Drawing.Point(396, 617);
            this.pictureBox_InvBluePotion.Name = "pictureBox_InvBluePotion";
            this.pictureBox_InvBluePotion.Size = new System.Drawing.Size(80, 80);
            this.pictureBox_InvBluePotion.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_InvBluePotion.TabIndex = 12;
            this.pictureBox_InvBluePotion.TabStop = false;
            this.pictureBox_InvBluePotion.Visible = false;
            // 
            // pictureBox_InvMace
            // 
            this.pictureBox_InvMace.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox_InvMace.Image = global::TheQuest.Properties.Resources.mace;
            this.pictureBox_InvMace.Location = new System.Drawing.Point(482, 617);
            this.pictureBox_InvMace.Name = "pictureBox_InvMace";
            this.pictureBox_InvMace.Size = new System.Drawing.Size(80, 80);
            this.pictureBox_InvMace.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_InvMace.TabIndex = 13;
            this.pictureBox_InvMace.TabStop = false;
            this.pictureBox_InvMace.Visible = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.labelPlayer, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.labelPlayerHP, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.labelBatHP, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.labelGhostHP, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.labelGhoulHP, 1, 3);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(716, 466);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(238, 97);
            this.tableLayoutPanel1.TabIndex = 14;
            // 
            // labelPlayer
            // 
            this.labelPlayer.AutoSize = true;
            this.labelPlayer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPlayer.Location = new System.Drawing.Point(3, 0);
            this.labelPlayer.Name = "labelPlayer";
            this.labelPlayer.Size = new System.Drawing.Size(58, 20);
            this.labelPlayer.TabIndex = 0;
            this.labelPlayer.Text = "Player";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Bat";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Ghost";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 72);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 20);
            this.label4.TabIndex = 3;
            this.label4.Text = "Ghoul";
            // 
            // labelPlayerHP
            // 
            this.labelPlayerHP.AutoSize = true;
            this.labelPlayerHP.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPlayerHP.Location = new System.Drawing.Point(122, 0);
            this.labelPlayerHP.Name = "labelPlayerHP";
            this.labelPlayerHP.Size = new System.Drawing.Size(109, 24);
            this.labelPlayerHP.TabIndex = 4;
            this.labelPlayerHP.Text = "labelPlayerHP";
            // 
            // labelBatHP
            // 
            this.labelBatHP.AutoSize = true;
            this.labelBatHP.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBatHP.Location = new System.Drawing.Point(122, 24);
            this.labelBatHP.Name = "labelBatHP";
            this.labelBatHP.Size = new System.Drawing.Size(99, 20);
            this.labelBatHP.TabIndex = 5;
            this.labelBatHP.Text = "labelBatHP";
            // 
            // labelGhostHP
            // 
            this.labelGhostHP.AutoSize = true;
            this.labelGhostHP.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelGhostHP.Location = new System.Drawing.Point(122, 48);
            this.labelGhostHP.Name = "labelGhostHP";
            this.labelGhostHP.Size = new System.Drawing.Size(109, 24);
            this.labelGhostHP.TabIndex = 6;
            this.labelGhostHP.Text = "labelGhostHP";
            // 
            // labelGhoulHP
            // 
            this.labelGhoulHP.AutoSize = true;
            this.labelGhoulHP.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelGhoulHP.Location = new System.Drawing.Point(122, 72);
            this.labelGhoulHP.Name = "labelGhoulHP";
            this.labelGhoulHP.Size = new System.Drawing.Size(108, 25);
            this.labelGhoulHP.TabIndex = 7;
            this.labelGhoulHP.Text = "labelGhoulHP";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::TheQuest.Properties.Resources.dungeon600x400;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1184, 761);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.pictureBox_InvMace);
            this.Controls.Add(this.pictureBox_InvBluePotion);
            this.Controls.Add(this.pictureBox_InvBow);
            this.Controls.Add(this.pictureBox_InvRedPotion);
            this.Controls.Add(this.pictureBox_InvSword);
            this.Controls.Add(this.pictureBox_player);
            this.Controls.Add(this.pictureBox_bat);
            this.Controls.Add(this.pictureBox_ghoul);
            this.Controls.Add(this.pictureBox_ghost);
            this.Controls.Add(this.pictureBox_bow);
            this.Controls.Add(this.pictureBox_bluepotion);
            this.Controls.Add(this.pictureBox_redpotion);
            this.Controls.Add(this.pictureBox_mace);
            this.Controls.Add(this.pictureBox_sword);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_sword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_mace)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_redpotion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_bluepotion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_bow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_ghost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_player)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_ghoul)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_bat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_InvSword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_InvRedPotion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_InvBow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_InvBluePotion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_InvMace)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox_sword;
        private System.Windows.Forms.PictureBox pictureBox_mace;
        private System.Windows.Forms.PictureBox pictureBox_redpotion;
        private System.Windows.Forms.PictureBox pictureBox_bluepotion;
        private System.Windows.Forms.PictureBox pictureBox_bow;
        private System.Windows.Forms.PictureBox pictureBox_ghost;
        private System.Windows.Forms.PictureBox pictureBox_player;
        private System.Windows.Forms.PictureBox pictureBox_ghoul;
        private System.Windows.Forms.PictureBox pictureBox_bat;
        private System.Windows.Forms.PictureBox pictureBox_InvSword;
        private System.Windows.Forms.PictureBox pictureBox_InvRedPotion;
        private System.Windows.Forms.PictureBox pictureBox_InvBow;
        private System.Windows.Forms.PictureBox pictureBox_InvBluePotion;
        private System.Windows.Forms.PictureBox pictureBox_InvMace;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label labelPlayer;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labelPlayerHP;
        private System.Windows.Forms.Label labelBatHP;
        private System.Windows.Forms.Label labelGhostHP;
        private System.Windows.Forms.Label labelGhoulHP;
    }
}

