﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
namespace TheQuest
{
    class Ghost:Enemy
    {
        public Ghost(Game game, Point Location)
            :base(game, Location, 8)
        {

        }

        public override void Move(Random random)
        {
            if(this.Hitpoints > 0)
            {
                if (random.Next(3) > 1)
                {
                    this.Move(FindPlayerDirection(game.PlayerLocation), game.Boundaries);
                }
            }
            else
            {

            }
        }
    }
}
